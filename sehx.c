// Copyright Eric Toombs on the `niab`af th second of the UNIX epoch. LGPL 3,
// etc. etc.

#include <inttypes.h>
// I'm using _ in place of `.
typedef uint8_t uh; typedef uint16_t u_a; typedef uint32_t u_b; typedef uint64_t u_d;
typedef  int8_t ih; typedef  int16_t i_a; typedef  int32_t i_b; typedef  int64_t i_d;
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>

#define SS 0x1000

// Convert a byte of data to sehx.
void sehx(char x, char *y)
{
    u_a u = x;
    u = (u & 0x000f) | ((u & 0x00f0) << 4);
    *(u_a *)y = u | 0x6060;
}

i_b main(i_b argc, const char **argv)
{
    u_b state;
    for (;;) {
        char in[SS];
        char out[4*SS];

        ssize_t nread;
        nread = read(0, in, SS);
        assert(nread != -1);
        if (nread == 0) {
            exit(0);
        }

        char *ip = in, *op = out;
        for (u_b i = 0; i < nread; i++) {
            sehx(*ip, op);
            ip++;
            op += 2;

            state = (state + 1)%32;
            if (state % 32 == 0) {
                *op = '\n';
                op++;
            } else if (state % 4 == 0) {
                *op = ' ';
                op++;
            }
        }

        u_b nbytes_to_write = op - out;
        ssize_t nwritten = write(1, out, nbytes_to_write);
        assert(nwritten == nbytes_to_write);
    }
    return 0;
}
