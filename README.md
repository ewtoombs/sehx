# sehx

*SEnsible HeXadecimal, the ASCII-synergetic little-endian hexadecimal format.*

It's a human-readable format that works *with* the conventions of ASCII and
little-endian, instead of *against* them. It is pronounced /sehx/ using the
International Phoenetic Alphabet.

## Rules
```
`. The bytes are displayed least-significant byte first *and* least-significant
   *nybble* first.

a. Digits are mapped linearly to ASCII, with digit zero mapping to the
   backtick, '`' = `f. This maps digit one through fifteen to the range of
   lowercase a to o.
```

## Rationale

As a practical matter, numbers in hex dumps are almost always little-endian.
Rule \` was chosen so that no byte swapping in sehx dumps is required in order to
see and understand the little-endian numbers, regardless of how many bits long
the numbers are. You do have to learn how to read sehx, but rule a helps with
this.

Rule a was chosen so that there would be no conceptual discontinuity in the
precession of one digit to another, like there is from 0x9 to 0xa. There is
also no chance of confusion with decimal because decimal digits aren't being
used at all. This rule was also chosen so that long strings of zeros would be
represented with just a bunch of itty bitty backticks, making it much easier to
see where the data is.

## Example

Here is a hex dump out of a Moto G Play (2021) guamna stock ROM:
```
414c5030 0000000a 00000080
d1b755f3 89bc1894 65341bb2 ec71bde7 7c2e8409 b95086ba 2b220433 24944473
00000268
889bf5e8 58097f01 1ba3ea00 4dc27cb5 fc8cc600 a9490db5 a8746d67 9eb45141

00000000 00000006 00000034
00000138 00000004 00000018
00000198 00000003 00000030
00000228 00000001 00000040

74737973 615f6d65 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000001 00000000 00000001 00000001
74737973 625f6d65 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000001 00000001 00000001 00000002
646e6576 615f726f 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000001 00000002 00000001 00000001
646e6576 625f726f 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000001 00000003 00000000 00000002
646f7270 5f746375 00000061 00000000 00000000 00000000 00000000 00000000 00000000 00000001 00000003 00000001 00000001
646f7270 5f746375 00000062 00000000 00000000 00000000 00000000 00000000 00000000 00000001 00000004 00000000 00000002

0019b558 00000000 00000000 00000800 00000000 00000000
0000b3a8 00000000 00000000 0019c000 00000000 00000000
0013d2d8 00000000 00000000 001a7800 00000000 00000000
0059cf00 00000000 00000000 002e5000 00000000 00000000

61666564 00746c75 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000
5f746f6d 675f7064 70756f72 0000615f 00000000 00000000 00000000 00000000 00000000 00000000 42b80000 00000001
5f746f6d 675f7064 70756f72 0000625f 00000000 00000000 00000000 00000000 00000000 00000000 42b80000 00000001

00000800 00000000 00100000 00000000 85f00000 00000002 65707573 00000072
```
Extra whitespace has been added to better reveal the hex dump's data
structures. The bytes have been swapped on \`b bit words. The ASCII characters
are thus in the wrong order.

Here is a sehx dump of that same data:
```
`c`eldad j``````` `h``````
coeegkam dihalkih bkkadcef gnmkagln i`dhnblg jkfh`eik ccd`bbkb cgdddidb
hfb`````
hneokihh a`ogi`he ``jncjka eklgblmd ``fllhlo ekm`idij gfmfdghj adaedkni

```````` f``````` dc``````
hca````` d``````` ha``````
hia````` c``````` `c``````
hbb````` a``````` `d``````

cgigcgdg efmfoeaf ```````` ```````` ```````` ```````` ```````` ```````` ```````` a``````` ```````` a``````` a```````
cgigcgdg efmfoebf ```````` ```````` ```````` ```````` ```````` ```````` ```````` a``````` a``````` a``````` b```````
fgefnfdf ofbgoeaf ```````` ```````` ```````` ```````` ```````` ```````` ```````` a``````` b``````` a``````` a```````
fgefnfdf ofbgoebf ```````` ```````` ```````` ```````` ```````` ```````` ```````` a``````` c``````` ```````` b```````
`gbgofdf egcfdgoe af`````` ```````` ```````` ```````` ```````` ```````` ```````` a``````` c``````` a``````` a```````
`gbgofdf egcfdgoe bf`````` ```````` ```````` ```````` ```````` ```````` ```````` a``````` d``````` ```````` b```````

heekia`` ```````` ```````` ``h````` ```````` ````````
hjck```` ```````` ```````` ```lia`` ```````` ````````
hmbmca`` ```````` ```````` ``hgja`` ```````` ````````
``olie`` ```````` ```````` ```enb`` ```````` ````````

dfefffaf eglfdg`` ```````` ```````` ```````` ```````` ```````` ```````` ```````` ```````` ```````` ````````
mfofdgoe df`goegf bgofeg`g oeaf```` ```````` ```````` ```````` ```````` ```````` ```````` ````hkbd a```````
mfofdgoe df`goegf bgofeg`g oebf```` ```````` ```````` ```````` ```````` ```````` ```````` ````hkbd a```````

``h````` ```````` `````a`` ```````` `````oeh b``````` cgeg`gef bg``````
```
No swapping of the bytes is necessary, because the nybbles are in little-endian
order, so they're already legible. And since the bytes weren't swapped, the
ASCII is in the right order. And the backticks really make the zeros in the hex
dump look very obnoxious by comparison. One other thing jumps out:
`` ````hkbd a``````` `` may actually be a \`d bit number. Indeed, it is.
This detail is much harder to notice in the hex dump, again due to the byte swapping.

## But wait! There's more!

There are a few accidental and totally awesome alignments in the sehx dumps of
ASCII text that were merely a result of having chosen the laziest possible
implementation of a system guided by the above rationale.

```
'    ' = `b`b`b`b
'`abcdefghijklmnopqrs' = `fafbfcf dfefffgf hfifjfkf lfmfnfof `gagbgcg
'@ABCDEFGHIJKLMNOPQRS' = `dadbdcd ddedfdgd hdidjdkd ldmdndod `eaebece
'0123456789' = `cacbccc dcecfcgc hcic
```

Also in the category of joyful side effects, the letters of the formatted data
are no longer limited to the range of `0xa` to `0xf`. Thus, much more
entertaining messages can be written into sehx dumps than were ever possible in
hex dumps, especially when backticks are used as spaces. Here are the top ooa
(according to the Wiktionary TV List) sehx-compatible English words, to get you
started.

First place:
> i 

The next b:
> a and 

The next d:
> of me in i\`m 

The next h:
> no do be on all he oh going 

The next \`a:
> like if can go him one did come good look back mean i\`ll been had ok 

The next \`b:
> an make need gonna am man life off doing god find again call feel fine home
> i\`d long big kind made being hi bad mom dad nice done mind hell came jack

The next \`d:
> idea coming old looking hello name hmm job ah dead alone kill hold deal once
> gone called head face each chance j making damn check change end ha ago kid
> glad feeling hand mine killed able die mm office cool child half men \`em bed
> line leo behind high ahead he\`ll game along book blood kidding lie coffee
> john jen hang ooh handle c

The next \`h:
> d needed o died jake al changed michael choice calling cold food none looked
> mad dance ben decided black fall dog he\`d e calm imagine b blame clean
> million become middle bo angel nick jail kinda college bill m fell mmm
> holding ice mama lied adam bag mike ball chloe join joke magic jim cell lead
> killing beginning liked doc hide kendall nine age hanging chief ho hal c\`mon
> decide fool alan joe ma\`am bob kick demon diane medical knock eh belle l
> king machine named ian mac goin\` checked begin abigail anna mail hiding chad
> la n bank finding colonel heh locked cake mood bianca belong ohh neck land
> eddie file lock legal fill gee monica jill band neil field doin hall dealing
> meaning chicken elaine deacon

The next \`\`a:
> colleen goddamn lab naked held common falling dancing ahem nah final michelle
> code ned bonnie chicago film hole o\`clock ended hank damage g beach mia gold
> checking leg colin amanda f caleb blind nicole add moon beg diego cole ahh
> fake de bomb h blake local hook jamal dean gia holden became feed engaged
> headed knife fan helena milk hm animal cook babe main daniel da blah block
> aah oakdale medicine jan cab don official lee dick donna malcolm lake clock
> dig indeed changing filled kicked ma annie jackie maggie heading oil failed
> doll edge bike hoo bone meal bail non bleeding fbi diana model gimme abe dan
> manage kane hilda cabin odd managed chef london cleaning male aidan knocked
> heck laid nail hill lane bond becoming damned dna claim chick kim among inn
> coincidence cancel illegal glen con maid helen begging nikki defend cooking
> coach confidence image jane coma bell mimi led ill logan gang fail challenge
> clinic handled load ocean ending command handed female mall mile manning
> diamond jamie comin chill blonde mel lookin egg joking alien lame hidden del
> co chain boom england blaming loan fallen ad lack alice heal defending
> alcohol cleaned fella hooked headache andie golden badge kicking hon cable
> leading abandoned china agenda linda began golf niki channel emma deck
> biological bobbie goal id gina gig handling dahlia gain fed candle media
> financial demand ali nina bang loaded childhood balance mob hah ego noble
> indian facing engine booked boo cookie digging hmmm magical amen joined inch
> flag canada beef el
